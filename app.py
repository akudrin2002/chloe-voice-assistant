import json
import queue
from random import choice

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
import sounddevice as sd
import vosk
import words
from skills import *
import voice
import git_skills

q = queue.Queue()

model = vosk.Model('vosk_model')

device = sd.default.device
samplerate = int(sd.query_devices(device[0], 'input')['default_samplerate'])


def callback(indata, frames, time, status):
    """
    Добавляет в очередь семплы из потока.
    вызывается каждый раз при наполнении blocksize
    в sd.RawInputStream"""

    q.put(bytes(indata))


def recognize(data, vectorizer, clf, path_for_git):
    """
    Анализ распознанной речи
    """

    # проверяем есть ли имя бота в data, если нет, то return
    trg = words.TRIGGERS.intersection(data.split())
    if not trg:
        return

    # удаляем имя бота из текста
    data = data.replace(list(trg)[0], '')
    if data == "":
        name_answers = [
            "Что вы хотели?",
            "Да-да?",
            "Слушаю",
            "Я тут, не переживайте"
        ]
        voice.speaker(choice(name_answers))
        return

    # получаем вектор полученного текста
    # сравниваем с вариантами, получая наиболее подходящий ответ
    text_vector = vectorizer.transform([data]).toarray()[0]
    answer = clf.predict([text_vector])[0]

    # получение имени функции из ответа из data_set
    func_name = answer.split()[0]

    # озвучка ответа из модели data_set
    voice.speaker(answer.replace(func_name, ''))
    print(func_name)

    if func_name.startswith("git"):
        git_helper = git_skills.GitHelper(path_for_git, func_name)  # GIT MODE
        print(git_helper.func_name)
        git_helper.act()
        return

    # запуск функции из skills
    exec(func_name + '()')


def main(path):
    """
    Обучаем матрицу ИИ
    и постоянно слушаем микрофон
    """

    # Обучение матрицы на data_set модели
    vectorizer = CountVectorizer()
    vectors = vectorizer.fit_transform(list(words.data_set.keys()))

    clf = LogisticRegression()
    clf.fit(vectors, list(words.data_set.values()))

    del words.data_set

    # постоянная прослушка микрофона
    with sd.RawInputStream(samplerate=samplerate, blocksize=16000, device=device[0], dtype='int16',
                           channels=1, callback=callback):

        rec = vosk.KaldiRecognizer(model, samplerate)
        while True:
            data = q.get()
            if rec.AcceptWaveform(data):
                data = json.loads(rec.Result())['text']
                print(data)
                recognize(data, vectorizer, clf, path)
            # else:
            #     print(rec.PartialResult())


if __name__ == '__main__':
    project_path = sys.argv[1]
    main(project_path)

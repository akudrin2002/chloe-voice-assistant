import os
import webbrowser
import sys
import subprocess

import voice
import requests
import pyowm
from pyowm.utils.config import get_default_config


def browser():
    """Открывает браузер заданнный по уполчанию в системе с url указанным здесь"""

    webbrowser.open('https://www.youtube.com', new=2)


def weather():
    """Для работы этого кода нужно зарегистрироваться на сайте
    https://openweathermap.org"""
    try:
        config_dict = get_default_config()
        config_dict["language"] = 'ru'
        owm = pyowm.OWM("760e5373e79937ba38e26c4944309135", config_dict)
        manager = owm.weather_manager()
        observation = manager.weather_at_place('Moscow,RU')
        weather_obj = observation.weather
        print(weather_obj.status)
        voice.speaker(
            f"На улице {weather_obj.detailed_status} {round(weather_obj.temperature('celsius')['temp'])} градусов"
        )

    except requests.RequestException:
        voice.speaker('Произошла ошибка при попытке запроса к ресурсу API, проверь код')


def offBot():
    """Отключает бота"""
    sys.exit()


def passive():
    """Функция заглушка при простом диалоге с ботом"""
    pass


def terminal():
    os.system("terminator")


def pycharm():
    os.system("pycharm-community")


def telegram():
    os.system("telegram-desktop")

def schedule():
    os.system("xdg-open ~/Desktop/schedule")

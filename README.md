
Установка синтезатора речи: 
```bash
sudo add-apt-repository ppa:linvinus/rhvoice
sudo apt-get update
sudo apt-get install rhvoice rhvoice-russian
sudo apt-get install speech-dispatcher-rhvoice

```
Модель для распознавания речи, распаковать в папку проекта:
    https://alphacephei.com/vosk/models
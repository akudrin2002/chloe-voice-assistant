import git
import os
import voice
import sounddevice as sd
import vosk
import json
import queue


class GitHelper:
    def __init__(self, path_to_project, func_name):
        self.q = None
        self.path = path_to_project
        self.func_name = func_name
        self.repo = git.Repo(os.path.abspath(self.path))

    def act(self):
        #try:
        if self.func_name == "git_add":
            self.repo.git.add("*")
        elif self.func_name == "git_status":
            print(self.repo.git.status())
        elif self.func_name == "git_commit":
            self.commit()
        #except:
            #voice.speaker("Произошла ошибка")
    
    def commit(self):
        self.q = queue.Queue()

        model = vosk.Model('vosk_model')

        device = sd.default.device
        samplerate = int(sd.query_devices(device[0], 'input')['default_samplerate'])

        while True:
            with sd.RawInputStream(samplerate=samplerate, blocksize=300000, device=device[0], dtype='int16',
                                   channels=1, callback=self.callback):

                rec = vosk.KaldiRecognizer(model, samplerate)
                data = self.q.get()
                if rec.AcceptWaveform(data):
                    data = json.loads(rec.Result())['text']
                    print("for commit: ", data)
                self.repo.index.commit(data)
                voice.speaker(f"текст коммита: {data}, подтверждаете?")

                data = str(input())
                if data == "да":
                    self.repo.index.commit(data)
                    voice.speaker(f"создан коммит {data}")
                    break
                print("Слушаю сообщение")

    def callback(self, indata, frames, time, status):
        """
        Добавляет в очередь семплы из потока.
        вызывается каждый раз при наполнении blocksize
        в sd.RawInputStream"""

        self.q.put(bytes(indata))


if __name__ == "__main__":
    repo = git.Repo("../python-kr2")
    repo.git.add("*")
    print(repo.git.status())
    #os.system(f"cd '../python-kr2' | git status")
